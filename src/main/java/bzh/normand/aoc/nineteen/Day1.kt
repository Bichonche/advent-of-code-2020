package bzh.normand.aoc.nineteen

import bzh.normand.aoc.utils.AoC
import kotlin.math.truncate

class Day1 : AoC(day = 1, year = 2019) {
    val test = """
        12
        14
        1969
        100756
    """.trimIndent()

    override fun partOne() {
        println(truncate(14 / 3.0))
        println(input.lineSequence().map { it.toDouble() }
                    .map { truncate(it / 3.0) - 2 }
                    .map { it.toLong() }
                    .onEach { println(it) }
                    .sum()
        )


    }

    override fun partTwo() {

        println("part 2")
        println(input.lineSequence().map { it.toDouble() }
                    .map {
                        var total = 0.0
                        var fuelToAdd = (truncate(it / 3.0) - 2)
                        while (fuelToAdd >= 0) {
                            total += fuelToAdd
                            fuelToAdd = (truncate(fuelToAdd / 3.0) - 2)
                        }
                        total
                    }
                    .map { it.toLong() }
                    .onEach { println(it) }
                    .sum())
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day1().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}