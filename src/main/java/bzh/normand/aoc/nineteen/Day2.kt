package bzh.normand.aoc.nineteen

import bzh.normand.aoc.utils.AoC

class Day2 : AoC(day = 2, year = 2019) {


    override fun partOne() {
        val list = input.split(",").map { it.toInt() }.toMutableList()
        hello(list, 2, 12)
        println(list[0])
    }

    private fun hello(list: MutableList<Int>, noun: Int, verb: Int) {
        var i1 = 0
        list[1] = noun
        list[2] = verb
        while (list[i1] != 99) {
            if (list[i1] == 1) list[list[i1 + 3]] = list[list[i1 + 1]] + list[list[i1 + 2]]
            else if (list[i1] == 2) list[list[i1 + 3]] = list[list[i1 + 1]] * list[list[i1 + 2]]
            i1 += 4
        }
        if (list[0] == 19690720) println(100 * noun + verb)
    }

    override fun partTwo() {
        for (noun in 0..99) {
            for (verb in 0..99)
                hello(list = input.split(",").map { it.toInt() }.toMutableList(), noun = noun, verb = verb)
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day2().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}