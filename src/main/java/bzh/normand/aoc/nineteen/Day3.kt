package bzh.normand.aoc.nineteen

import bzh.normand.aoc.utils.AoC

class Day3 : AoC(year = 2019, day = 3) {
    override fun partOne() {
        val list = mutableMapOf<Pair<Int, Int>, Int>()
        var currentX = 0
        var currentY = 0
        input.lines().forEach { string ->
            string.split(",").onEach { println("oui: $it") }.forEach {
                Regex("^([A-Z])([0-9]*)$")
                    .findAll(it)
                    .first()
                    .groupValues.let { values: List<String> ->
                        val move = values[2].toInt()
                        var newX = currentX
                        var newY = currentY
                        when (values[1]) {
                            "R" -> newX += move
                            "L" -> newX -= move
                            "U" -> newY += move
                            "D" -> newY -= move
                            else -> throw Error()
                        }
                        for (x in currentX..newX)
                            for (y in currentY..newY)
                                list[x to y] = (list[x to y] ?: 0) + 1
                    }
            }
        }
        list.filter { it.value >= 1 }.onEach { println(it) }
    }

    override fun partTwo() {
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day3().let {
                it.partOne()
                it.partTwo()
            }
        }
    }

}