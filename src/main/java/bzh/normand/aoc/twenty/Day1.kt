package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.streams.toList

class Day1 {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines = Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day1/input.txt")
                    .bufferedReader()
                    .lines()
                    .map { it.toInt() }
                    .toList()
            for (s: Int in lines) {
                for (s2: Int in lines) {
                    for (s3: Int in lines) {
                        if (s + s2 + s3 == 2020) {
                            println(s * s2 * s3)
                            break
                        }
                    }
                }
            }
        }
    }
}