package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC
import java.lang.Math.pow

class Day10 : AoC(day = 10, year = 2020) {

    val test1 = """16
        10
        15
        5
        1
        11
        7
        19
        6
        12
        4""".trimIndent()

    val test = """ 28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
""".trim()

    override fun partOne() {
        val sorted = input.lines().map { it.trim().toInt() }.sorted()
        val min = sorted.first()
        val map = mutableMapOf<Int, Int>()

        map[min] = 1
        for (i in 0..sorted.size - 2) {
            when (sorted[i + 1] - sorted[i]) {
                1 -> map[1] = (map[1] ?: 0) + 1
                3 -> map[3] = (map[3] ?: 0) + 1
            }
        }
        println(map[1]!! * (map[3]!! + 1))
    }

    override fun partTwo() {
        val map = input.lines().map { it.trim().toInt() }.toMutableList()
        map.addAll(listOf(0))
        val sorted = map.sorted().toMutableList()
        println("size : ${sorted.size}")
        println("list : $sorted")

        println(compute(sorted))


    }


    private fun compute(list: List<Int>): Long {
        if (list.size <= 1) {
            return 1
        }
        var count = 0L
        val index = list[0]
        val copy = list.toMutableList().apply {
            this.remove(index)
        }
        val valid = copy.filter { it <= index + 3 }
        for (i in valid.indices) {
            if (i != 0) {
                copy.remove(valid[i - 1])
            }
            count += values.getOrPut(copy, { compute(copy) })
        }
        return count

    }

    private val values = mutableMapOf<List<Int>, Long>()


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day10().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}