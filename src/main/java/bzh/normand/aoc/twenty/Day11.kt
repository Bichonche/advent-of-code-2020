package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day11 : AoC(year = 2020, day = 11) {
    val test = """
        L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL
    """.trimIndent()

    override fun partOne() {
        val toList = input.lineSequence().map { it.toList() }.toList()

        println(count(toList))


    }

    private fun <T> count(list: List<List<T>>): Int {
        var count = 0
        val newList = list.toMutableList().mapIndexed { i, l ->
            l.mapIndexed { j, c ->
                when (c) {
                    'L' -> {
                        if (list.getAdjacent(i, j).filter { it == '#' }.none()) {
                            count++
                            '#'
                        } else {
                            'L'
                        }
                    }
                    '#' -> {
                        if (list.getAdjacent(i, j).filter { it == '#' }.count() >= 4) {
                            count++
                            'L'
                        } else {
                            '#'
                        }
                    }
                    else -> {
                        '.'
                    }
                }
            }
        }
        println("=======")
        newList.forEach { it.forEach { c -> print(c) }; println() }
        if (count == 0) {

            return newList.flatMap { it.toList() }.count { it == '#' }
        }

        return count(newList)
    }

    private fun <T> count2(list: List<List<T>>): Int {
        var count2 = 0
        val newList = list.toMutableList().mapIndexed { i, l ->
            l.mapIndexed { j, c ->
                when (c) {
                    'L' -> {
                        if (list.getAdjacent2(i, j).filter { it == '#' }.none()) {
                            count2++
                            '#'
                        } else {
                            'L'
                        }
                    }
                    '#' -> {
                        if (list.getAdjacent2(i, j).filter { it == '#' }.count() >= 5) {
                            count2++
                            'L'
                        } else {
                            '#'
                        }
                    }
                    else -> {
                        '.'
                    }
                }
            }
        }
        println("=======")
        newList.forEach { it.forEach { c -> print(c) }; println() }
        if (count2 == 0) {
            return newList.flatMap { it.toList() }.count { it == '#' }
        }

        return count2(newList)
    }

    private fun <T> List<List<T>>.getAdjacent(i: Int, j: Int): List<T> {
        val list = mutableListOf<T>()
        if (i > 0) {
            list.add(this[i - 1][j])
            if (j > 0) list.add(this[i - 1][j - 1])
            if (j < this[i].size - 1) list.add(this[i - 1][j + 1])
        }
        if (i < this.size - 1) {
            list.add(this[i + 1][j])
            if (j > 0) list.add(this[i + 1][j - 1])
            if (j < this[i].size - 1) list.add(this[i + 1][j + 1])
        }
        if (j > 0) list.add(this[i][j - 1])
        if (j < this[i].size - 1) list.add(this[i][j + 1])
        return list
    }

    private fun <T> List<List<T>>.getAdjacent2(i: Int, j: Int): List<T> {
        val list = mutableListOf<T>()
        for (r in listOf(-1, 0, 1)) {
            for (c in listOf(-1, 0, 1)) {
                if (c == 0 && r == 0) continue
                var tempi = i + r
                var tempj = j + c
                while ((tempi >= 0 && tempi < this.size) && (tempj >= 0 && tempj < this[i].size)) {
                    if (this[tempi][tempj] != '.') {
                        list.add(this[tempi][tempj])
                        break
                    }
                    tempj += c
                    tempi += r
                }
            }
        }
        return list
    }

    override fun partTwo() {
        val toList = input.lineSequence().map { it.toList() }.toList()
        println(count2(toList))
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day11().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}