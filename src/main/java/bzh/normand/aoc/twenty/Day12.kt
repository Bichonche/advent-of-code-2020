package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC
import kotlin.math.abs

class Day12 : AoC(day = 12, year = 2020) {

    val test = """
        F10
        N3
        F7
        R90
        F11
        L90
        F20
        R90
        F20
    """.trimIndent()

    override fun partOne() {

        var currentDir = "E"
        val map = mutableMapOf(
            "N" to 0,
            "W" to 0,
            "S" to 0,
            "E" to 0
        )
        var angle = 0
        val dirs = listOf("N", "W", "S", "E")
        val values = input.lineSequence()
            .map { it[0].toString() to it.substring(1 until it.length).toInt() }
            .forEach {
                when (it.first) {
                    "L" -> {
                        val abs = dirs[(dirs.indexOf(currentDir) + it.second / 90) % 4]
                        currentDir = abs
                        angle += it.second
                    }
                    "R" -> {
                        val s = dirs[(4 - (it.second / 90 - dirs.indexOf(currentDir))) % 4]
                        currentDir = s
                        angle -= it.second
                    }
                    "F" -> map[currentDir] = map[currentDir]!! + it.second
                    else -> map[it.first] = map[it.first]!! + it.second
                }
//                println(
//                    "x: ${map["E"]!! - map["W"]!!} y: ${map["N"]!! - map["S"]!!} dir: ${currentDir}, inst: $it"
//                )
            }

        println(abs(map["N"]!! - map["S"]!!) + abs(map["E"]!! - map["W"]!!))
    }

    override fun partTwo() {

        var waypointX = 10
        var waypointY = 1
        var posX = 0
        var posY = 0

        val values = input.lineSequence()
            .map { it[0].toString() to it.substring(1 until it.length).toInt() }
            .forEach {
                when (it.first) {
                    "R" -> {
                        for (i in 0 until it.second / 90) {
                            val temp = waypointX
                            waypointX = waypointY
                            waypointY = -temp
                        }
                    }
                    "L" -> {
                        for (i in 0 until it.second / 90) {
                            val temp = waypointX
                            waypointX = -waypointY
                            waypointY = temp
                        }
                    }
                    "F" -> {
                        for (i in 0 until it.second) {
                            posX += waypointX
                            posY += waypointY
                        }
                    }
                    "N" -> waypointY += it.second
                    "S" -> waypointY -= it.second
                    "W" -> waypointX -= it.second
                    "E" -> waypointX += it.second
                }
            }

        println(abs(posX) + abs(posY))
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day12().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}