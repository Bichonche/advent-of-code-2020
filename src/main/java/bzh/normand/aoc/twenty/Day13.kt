package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day13 : AoC(year = 2020, day = 13) {

    val test = """
        939
        7,13,x,x,59,x,31,19
    """.trimIndent()

    override fun partOne() {
        val timestamp = input.lines()[0].toInt()
        val buses = input.lines()[1]
            .split(",")
            .filter { it != "x" }
            .map { it.toInt() }
            .map { it to it - timestamp % it }
            .minByOrNull { it.second }!!

        println(buses.first * buses.second)
    }

    fun multInv(a: Int, b: Int): Int {
        if (b == 1) return 1
        var aa = a
        var bb = b
        var x0 = 0
        var x1 = 1
        while (aa > 1) {
            val q = aa / bb
            var t = bb
            bb = aa % bb
            aa = t
            t = x0
            x0 = x1 - q * x0
            x1 = t
        }
        if (x1 < 0) x1 += b
        return x1
    }

    private fun chineseRemainder(n: List<Int>, a: List<Int>): Int {
        val prod = n.fold(1) { acc, i -> acc * i }
        var sum = 0
        for (i in n.indices) {
            val p = prod / n[i]
            sum += a[i] * multInv(p, n[i]) * p
        }
        return sum % prod
    }

    override fun partTwo() {
        val buses = input.lines()[1]
            .split(",")
            .mapIndexed { k, v -> k to v }
            .filter { it.second != "x" }
            .map { it.first to it.second.toInt() }


        println(chineseRemainder(buses.map { it.first + it.second }, buses.map { it.second }))
        println(buses)
//        println(lcm(buses))
    }


    private fun lcm(a: Long, b: Long): Long {
        var lcm = if (a > b) a else b

        while (true) {
            if (lcm % a == 0L && lcm % b == 0L) {
                break
            }
            ++lcm
        }
        return lcm
    }

    private fun lcm(list: List<Long>): Long {
        if (list.size == 2) {
            return lcm(list[0], list[1])
        }
        return lcm(list[0], lcm(list.subList(1, list.size)))
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {


            Day13().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}