package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day14 : AoC(year = 2020, day = 14) {

    val test = """
       mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1
    """.trimIndent()

    override fun partOne() {


        val mem = mutableMapOf<Int, Long>()
        val lines = input.lines()

        var maskx = 0L
        var mask1 = 0L
        lines.forEach {
            it.split("=").run {
                when (this[0].trim()) {
                    "mask" -> {
                        maskx = 0L
                        mask1 = 0L
                        for (i in this[1].trim()) {
                            maskx = maskx shl 1
                            mask1 = mask1 shl 1
                            when (i) {
                                'X' -> maskx += 1
                                '1' -> mask1 += 1
                            }
                        }
                    }
                    else -> {
                        val memAddress = Regex("mem[\\[]([0-9]*)[]]").find(this[0])?.let { m ->
                            m.groupValues[1].toInt()
                        }!!
                        mem[memAddress] = (this[1].trim().toLong() and maskx) + mask1
                    }
                }
            }
        }



        println(mem.map { it.value }.sum())
    }

    override fun partTwo() {

        val mem = mutableMapOf<Long, Long>()
        val lines = input.lines()

        var masks = listOf<Long>()
        var mask2 = 0L
        var maskx = 0L
        lines.forEach {
            it.split("=").run {
                val value = this[1].trim()
                when (this[0].trim()) {
                    "mask" -> {
                        masks = listOf()
                        mask2 = 0L
                        maskx = 0L
                        for (i in value) {
                            mask2 = mask2 shl 1
                            maskx = maskx shl 1
                            when (i) {
                                'X' -> maskx += 1
                                '1' -> mask2 += 1
                            }
                        }
                        masks = getMasksFrom(value)
                    }
                    else -> {
                        val memAddress = Regex("mem[\\[]([0-9]*)[]]").find(this[0])?.let { m ->
                            m.groupValues[1].toInt()
                        }!!
                        masks.forEach { v ->
                            val l = ((memAddress.toLong() or maskx) - v) or mask2
                            mem[l] = value.toLong()
                        }
                    }
                }
            }
        }

        println(mem.map { it.value }.sum())
    }

    fun getMasksFrom(s: String): MutableList<Long> {

        var list = mutableListOf(0L)

        for (i in s) {
            if (i == 'X') {
                list = list.map { listOf((it shl 1) + 1, it shl 1) }.flatten().toMutableList()
            } else {
                list = list.map { it shl 1 }.toMutableList()
            }
        }
        return list
    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day14().let {
//                it.partOne()
                it.partTwo()
            }
        }
    }
}