package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day15 : AoC(year = 2020, day = 15) {

    val test = "0,3,6"

    override fun partOne() {
        println(getNthTurn(2020))
    }

    override fun partTwo() {
        println(getNthTurn(30_000_000))
    }

    private fun getNthTurn(int: Int): Int {
        val split = input.split(",")
        val map = split.mapIndexed { k, v -> v.toInt() to Pair<Int, Int?>(k, null) }.toMap().toMutableMap()
        val list = split.map { it.toInt() }.toMutableList()
        var i = map.size
        while (i <= (int - 1)) {
            val precedingValue = list[i - 1]
            val indexes = map[precedingValue]!!
            val value: Int = if (indexes.second != null) {
                indexes.first - indexes.second!!
            } else {
                0
            }
            list.add(i, value)
            map[value] = Pair(i, map[value]?.first)
            i++
        }

        return list[int - 1]
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day15().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}