package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day16 : AoC(year = 2020, day = 16) {
    val test = """
       class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
99,99,99
3,9,18
15,1,5
5,14,9
    """.trimIndent()

    override fun partOne() {
        var counter = 0
        val map = mutableMapOf<String, Pair<Pair<Int, Int>, Pair<Int, Int>>>()
        val myTicket = mutableListOf<Int>()
        val nearbyTickets = mutableListOf<List<Int>>()
        var step = 1
        val lines = test.lines()
        for (line in lines.indices step step) {
            when (counter) {
                0 -> {
                    if (lines[line].isEmpty()) {
                        counter++
                        continue
                    }
                    lines[line].split(":").let {
                        map.put(it[0], it[1].trim().split("or").let { value ->
                            value[0].trim().split("-").let { left -> Pair(left[0].toInt(), left[1].toInt()) } to
                                    value[1].trim().split("-").let { right -> Pair(right[0].toInt(), right[1].toInt()) }
                        })
                    }
                }
                1 -> {
                    if (lines[line].isEmpty()) {
                        counter++
                        continue
                    }
                    if (lines[line].contains(','))
                        myTicket.addAll(lines[line].split(",").map { it.toInt() })
                }
                2 -> {
                    if (lines[line].contains(','))
                        nearbyTickets.add(lines[line].split(",").map { it.toInt() })
                }
            }
        }

        var res = 0
        for (ticket in nearbyTickets) {
            for (value in ticket) {
                var isvalid = false
                for (ranges in map.values) {
                    if ((value in ranges.first.first..ranges.first.second || value in ranges.second.first..ranges.second.second))
                        isvalid = true
                }
                if (!isvalid) res += value
            }
        }

        println(res)

    }

    override fun partTwo() {
        var counter = 0
        val map = mutableMapOf<String, Pair<Pair<Int, Int>, Pair<Int, Int>>>()
        val myTicket = mutableListOf<Int>()
        val nearbyTickets = mutableListOf<List<Int>>()
        var step = 1
        val lines = input.lines()
        for (line in lines.indices step step) {
            when (counter) {
                0 -> {
                    if (lines[line].isEmpty()) {
                        counter++
                        continue
                    }
                    lines[line].split(":").let {
                        map.put(it[0], it[1].trim().split("or").let { value ->
                            value[0].trim().split("-").let { left -> Pair(left[0].toInt(), left[1].toInt()) } to
                                    value[1].trim().split("-").let { right -> Pair(right[0].toInt(), right[1].toInt()) }
                        })
                    }
                }
                1 -> {
                    if (lines[line].isEmpty()) {
                        counter++
                        continue
                    }
                    if (lines[line].contains(','))
                        myTicket.addAll(lines[line].split(",").map { it.toInt() })
                }
                2 -> {
                    if (lines[line].contains(','))
                        nearbyTickets.add(lines[line].split(",").map { it.toInt() })
                }
            }
        }


        val validTickets = ArrayList<List<Int>>()
        val repartition = HashMap<String, HashMap<Int, Int>>()
        for (ticket in nearbyTickets) {
            var setValidValues = hashSetOf<Int>()
            for (index in ticket.indices) {
                for (ranges in map.values) {
                    if ((ticket[index] in ranges.first.first..ranges.first.second || ticket[index] in ranges.second.first..ranges.second.second))
                        setValidValues.add(index)
                }
            }
            if (setValidValues.size == ticket.size) validTickets.add(ticket)
        }

        for (ticket in validTickets) {
            for (index in ticket.indices) {
                for (entry in map) {
                    if ((ticket[index] in entry.value.first.first..entry.value.first.second ||
                                ticket[index] in entry.value.second.first..entry.value.second.second)
                    ) {
                        val value = repartition.getOrPut(entry.key) { HashMap() }
                        value[index] = (value[index] ?: 0) + 1
                    }
                }
            }
        }

        val message = repartition.map { name ->
            name.key to name.value
                .map { it.key to it.value }
                .filter { it.second == validTickets.size }
        }
        println(repartition)
        val set = hashSetOf<Int>()
        val soluce = HashMap<String, Int>()
        for (i in 0..100) {

            val filter = message.map { it.first to it.second.filter { pair -> !set.contains(pair.first) } }
            val first = filter.first { it.second.size == 1 }
            set.add(first.second.first().first)
            soluce[first.first] = first.second.first().first
            if (set.size == myTicket.size) break
        }
        println(soluce)
        val indicesToAdd = soluce.filter { it.key.startsWith("departure") }.map { it.value }
        println(indicesToAdd)
        val result = myTicket.filterIndexed { index, i -> indicesToAdd.contains(index) }
        var r = 1L
        for (i in result) {
            println(i)
            r *= i
        }
        println(r)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Day16().let {
                it.partOne()
                it.partTwo()
            }
        }
    }
}