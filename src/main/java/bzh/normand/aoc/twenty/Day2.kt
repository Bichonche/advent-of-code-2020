package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils

class Day2 {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines = Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day2/input.txt")
                    .bufferedReader()
                    .lines()


            val count = lines.filter { s: String ->
                val min: Int
                val max: Int
                val letter: Char
                val pass: String
                s.split(":").let {
                    pass = it[1].trim()
                    it[0].split(" ").let { left ->
                        letter = left[1].trim()[0]
                        left[0].split("-").let { minmax ->
                            min = minmax[0].toInt() - 1
                            max = minmax[1].toInt() - 1
                        }
                    }
                }
                (pass[min] == letter && pass[max] != letter) || (pass[min] != letter && pass[max] == letter)
            }.count()

            println(count)


        }
    }
}