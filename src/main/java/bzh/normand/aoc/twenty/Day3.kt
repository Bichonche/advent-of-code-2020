package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.streams.toList

class Day3 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day3/input.txt")
                    .bufferedReader()
                    .lines()

            val rows = lines.map { l -> l.toCharArray().asList() }.toList()
            println(
                countTrees(rows, right = 1, down = 1) *
                        countTrees(rows, right = 3, down = 1) *
                        countTrees(rows, right = 5, down = 1) *
                        countTrees(rows, right = 7, down = 1) *
                        countTrees(rows, right = 1, down = 2)
            )
        }

        private fun countTrees(rows: List<List<Char>>, right: Int, down: Int): Int {
            val width = rows[0].size
            var offset = 0
            var count = 0
            for (i in down until rows.size step down) {
                offset = (offset + right) % width
                if (rows[i][offset] == '#') count++
            }
            return count
        }
    }
}