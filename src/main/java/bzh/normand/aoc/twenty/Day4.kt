package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils

class Day4 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day4/input.txt")
                    .bufferedReader()
                    .lines()

            val set = setOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

            val passports = mutableListOf<Map<String, String>>()
            val passport = mutableMapOf<String, String>()
            for (line: String in lines) {
                if (line.isEmpty()) {
                    passports.add(passport)
                    passport.clear()
                } else {
                    passport.putAll(line.split(" ").map { el ->
                        el.split(":").let { it[0] to it[1] }
                    }.toMap())
                }
            }
            passports.add(passport)


            println(passports.filter { it.keys.containsAll(set) }.filter { map ->
                map["byr"]!!.toIntOrNull()?.let { it in 1920..2002 } ?: false &&
                        map["iyr"]!!.toIntOrNull()?.let { it in 2010..2020 } ?: false &&
                        map["eyr"]!!.toIntOrNull()?.let { it in 2020..2030 } ?: false &&
                        Regex("^([0-9]*)(cm|in)$").findAll(map["hgt"]!!).toList().firstOrNull()?.let {
                            if (it.groupValues.size == 3) {
                                when (it.groupValues[2]) {
                                    "cm" -> {
                                        it.groupValues[1].toIntOrNull()?.let { n -> n in 150..193 } ?: false
                                    }
                                    "in" -> {
                                        it.groupValues[1].toIntOrNull()?.let { n -> n in 59..76 } ?: false
                                    }
                                    else -> {
                                        false
                                    }
                                }
                            } else {
                                false
                            }
                        } ?: false &&
                        Regex("^[#][0-9a-f]{6}$").matches(map["hcl"]!!) &&
                        setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(map["ecl"]!!) &&
                        Regex("^[0-9]{9}$").matches(map["pid"]!!)
            }.count())
        }
    }
}