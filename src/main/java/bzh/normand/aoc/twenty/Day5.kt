package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.math.abs
import kotlin.math.max
import kotlin.streams.toList

class Day5 {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day5/input.txt")
                    .bufferedReader()
                    .lines()


            val func: (List<Char>) -> Int = {
                var rrow = 0
                var rcol = 0
                it.forEach { char: Char ->
                    when (char) {
                        'F' -> rrow = rrow shl 1
                        'B' -> rrow = (rrow shl 1) + 1
                        'L' -> rcol = rcol shl 1
                        'R' -> rcol = (rcol shl 1) + 1
                    }
                }
                rrow * 8 + rcol
            }

            val list = lines.map { func.invoke(it.toList()) }.toList()
            for (id1 in list) {
                for (id2 in list) {
                    if (abs(id1 - id2) == 2) {
                        val element = max(id1, id2) - 1
                        if (!list.contains(element)) {
                            return println("id1 : $id1, id2 : $id2, element : $element")
                        }
                    }
                }
            }
        }
    }
}