package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.streams.toList

class Day6 {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("F:/workspace/perso/advent_of_code/src/main/resources/files/day6/input.txt")
                    .bufferedReader()
                    .lines().toList()

            val list = mutableListOf<MutableSet<Char>>()
            var set = mutableSetOf<Char>()
            var start = true
            for (s in lines) {
                if (s.isEmpty()) {
                    list.add(set)
                    set = mutableSetOf()
                    start = true
                } else {
                    if (start) {
                        set.addAll(s.toList())
                        start = false
                    } else {
                        set.removeAll(set subtract s.toSet())
                        for (char in s.toList()) {
                            if (!set.contains(char)) set.remove(char)
                        }
                    }
                }
            }

            val sum = list.onEach { println(it) }.map { it.count() }.onEach { println(it) }.sum()

            println(sum)
        }
    }
}