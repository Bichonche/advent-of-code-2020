package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.streams.toList

class Day7 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("/home/corentin/workspace/perso/advent-of-code-2020/src/main/resources/files/day7/input.txt")
                    .bufferedReader()
                    .lines().toList()


            val entryList = lines.map { line ->
                Regex("^(.*) bags contain (.*)[.]").findAll(line).toList()[0].groupValues.let { values ->
                    values[1] to values[2].split(",").map { bagLine ->
                        Regex("^([0-9]*) (.*) ba(g|gs)$").findAll(bagLine.trim()).toList().let { results ->
                            if (results.isEmpty()) {
                                null
                            } else {
                                results[0].groupValues.let {
                                    it[2] to it[1].toInt()
                                }
                            }
                        }
                    }.mapNotNull { pair -> pair?.let { it.first to it.second } }.toMap()
                }
            }.toMap()

            println(entryList)

            fun count(s: String, int: Int = 0): Int {
                var counter = int
                counter += entryList[s]!!.let { entry ->
                    if (entry.isEmpty()) {
                        1
                    } else {
                        1 + entry.map { it.value * count(it.key) }.sum()
                    }
                }
                return counter
            }

            println(count("shiny gold") - 1)


        }

    }
}