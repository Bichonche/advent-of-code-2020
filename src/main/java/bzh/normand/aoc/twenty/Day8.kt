package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.Utils
import kotlin.streams.toList

class Day8 {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val lines =
                Utils.getInputStream("/home/corentin/workspace/perso/advent-of-code-2020/src/main/resources/files/day8/input.txt")
                    .bufferedReader()
                    .lines()
                    .map { it.split(" ") }
                    .filter { it.size == 2 }
                    .map { it[0] to it[1].toInt() }
                    .toList()

            val nopIndices = mutableListOf<Int>()
            val jmpIndices = mutableListOf<Int>()

            lines.forEachIndexed { index, pair ->
                if (pair.first == "nop") nopIndices.add(index)
                else if (pair.first == "jmp") jmpIndices.add(index)
            }

            for (i in nopIndices) {
                val newLines = lines.toMutableList()
                newLines[i] = "jmp" to newLines[i].second
                val isRepeating = isRepeating(newLines)
                if (!isRepeating.first) println(isRepeating.second)
            }

            for (i in jmpIndices) {
                val newLines = lines.toMutableList()
                newLines[i] = "nop" to newLines[i].second
                val isRepeating = isRepeating(newLines)
                if (!isRepeating.first) println(isRepeating.second)
            }


        }

        private fun isRepeating(lines: List<Pair<String, Int>>): Pair<Boolean, Int> {
            var acc = 0
            val set = mutableSetOf<Int>()
            var i = 0
            while (!set.contains(i)) {
                if (i == lines.size) return false to acc
                val pair = lines[i]

                set.add(i)

                when (pair.first) {
                    "acc" -> {
                        acc += pair.second
                        i++
                    }
                    "jmp" -> i += pair.second
                    else -> i++
                }
                if (set.contains(i)) {
                    break
                }

            }
            return true to acc
        }
    }
}