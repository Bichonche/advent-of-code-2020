package bzh.normand.aoc.twenty

import bzh.normand.aoc.utils.AoC

class Day9 : AoC(9, 2020) {
    val test = """
    35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576
""".trimIndent()


    override fun partOne() {
        val lines = input.lineSequence().map { it.toLong() }.toList()
        val test = test.lines().map { it.toLong() }
        getLastNotValidValue(25, lines)

    }

    private fun getLastNotValidValue(size: Int, lines: List<Long>): Long {
        for (i in 0..lines.size - (size + 1)) {
            val preamble = lines.subList(i, i + size)
            val next = lines[i + (size)]
            if (!isValid(preamble, next)) return next
        }
        return 0
    }


    fun isValid(preamble: List<Long>, next: Long): Boolean {
        for (j in preamble) {
            for (k in preamble) {
                if (j + k == next) return true
            }
        }
        return false
    }

    override fun partTwo() {
        val lines = input.lineSequence().map { it.toLong() }.toList()
        val theNumber = getLastNotValidValue(25, lines)
//        val lines = test.lines().map { it.toLong() }
        lines.forEachIndexed { i, long ->
            val first = long
            val numbers = mutableListOf(first)
            var number = long
            for (j in i + 1 until lines.size) {
                number += lines[j]
                numbers.add(lines[j])
                if (number == theNumber) {
                    println((numbers.maxOrNull() ?: 0) + (numbers.minOrNull() ?: 0))
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {

            Day9().let {
                println(it.partOne())
                println(it.partTwo())
            }
        }
    }
}