package bzh.normand.aoc.utils

import io.github.cdimascio.dotenv.Dotenv
import java.io.File

abstract class AoC(
    private val day: Int,
    private val year: Int,
) {

    private val dotenv = Dotenv.load()

    protected val input = getTheInput()


    private fun getTheInput(): String {
        File("files/$year/day$day/").mkdirs()
        val file = File("files/$year/day$day/input.txt")
        return file.let {
            if (it.createNewFile()) {
                it.writeText(
                    khttp.get(
                        "https://adventofcode.com/$year/day/$day/input",
                        cookies = mapOf("session" to dotenv["SESSION"])
                    ).text
                )
            }
            it.readText()
        }
    }


    abstract fun partOne()

    abstract fun partTwo()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            
        }
    }


}