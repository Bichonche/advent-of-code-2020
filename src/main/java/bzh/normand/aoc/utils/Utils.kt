package bzh.normand.aoc.utils

import java.io.File
import java.io.InputStream

class Utils {

    companion object {
        fun getInputStream(path: String): InputStream {
            return File(path).inputStream()
        }
    }
}